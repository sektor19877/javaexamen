<%-- 
    Document   : ingresarAlumno
    Created on : 06-05-2014, 03:52:42 PM
    Author     : Administrador
--%>
<%@page import="java.util.List"%>
<%@page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controlador.RazaDto" %>
<%@page import="Modelo.RazaDao" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% List<RazaDto> listaRaza= RazaDao.listaRazas();   %>
        <form action="ingresarParticipante" method="POST">
            <table border="0">
                
                <tbody>
                    <tr>
                        <td>Nombre Participante</td>
                        <td><input type="text" name="txtParticipante" value="" /></td>
                    </tr>
                    <tr>
                        <td>Raza</td>
                        <td> <select name="raza">
                                
                <option value="">Selecione una raza</option>
                <%for (RazaDto element : listaRaza) {   %>
                <option value="<%  Integer.toString(element.getIdRaza());   %>"><%element.getNombreRaza().toString();   %></option>
                <% }   %>
            </select></td>
                    </tr>
                    <tr>
                        <td>Id registro</td>
                        <td><input name="txtIdRegistro" rows="4" cols="20">
                            </input></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <input type="submit" value="Grabar" name="btnGrabar" />

        </form>
                        <td><a href="listarParticipantes.jsp">Listar Participantes</a></td>
    </body>
</html>
