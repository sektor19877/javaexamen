/*
SQLyog Trial v12.4.3 (64 bit)
MySQL - 5.5.24-log : Database - dej
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dej` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dej`;

/*Table structure for table `participante` */

DROP TABLE IF EXISTS `participante`;

CREATE TABLE `participante` (
  `id_participante` int(20) NOT NULL AUTO_INCREMENT,
  `nombre_participante` varchar(50) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_raza` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_participante`),
  KEY `fk_raza` (`id_raza`),
  CONSTRAINT `fk_raza` FOREIGN KEY (`id_raza`) REFERENCES `raza` (`id_raza`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `participante` */

insert  into `participante`(`id_participante`,`nombre_participante`,`id_registro`,`fecha_registro`,`id_raza`) values 
(3,'asd',1,'2017-10-09 00:00:00',1);

/*Table structure for table `raza` */

DROP TABLE IF EXISTS `raza`;

CREATE TABLE `raza` (
  `id_raza` bigint(20) NOT NULL,
  `nombre_raza` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_raza`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `raza` */

insert  into `raza`(`id_raza`,`nombre_raza`) values 
(1,'pitbull'),
(2,'doberman'),
(3,'quiltro');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
