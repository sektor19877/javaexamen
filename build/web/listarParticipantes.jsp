<%-- 
    Document   : listarResultado
    Created on : 25-04-2014, 02:56:33 PM
    Author     : cetecom
--%>

<%@page import="java.util.List"%>
<%@page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controlador.ParticipanteDto" %>
<%@page import="Controlador.RazaDto" %>
<%@page import="Modelo.ParticipanteDao" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Salida de query</title>
    </head>
    <body>
        <h1>Todos los registos</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>raza</th>
                    <th>registro</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <%List<ParticipanteDto> lista=Modelo.ParticipanteDao.listaParticipantes();
                        
                      %>
               
                 <%for(ParticipanteDto dto: lista){ %>                                
                <tr>
                     <td><%=  dto.getIdParticipante() %></td>
                    <td><%= dto.getNombre_participante() %></td>
                    <td><%= dto.getIdRaza() %></td>
                    <td><%= dto.getIdRegistro() %></td>
                    <td><a href="ControladorProducto?opc=delete&codigo=<%=dto.getIdRegistro()%>">Eliminar</a></td>
                </tr>
                <% } %>
            </tbody>
        </table>

        
        
    </body>
</html>
