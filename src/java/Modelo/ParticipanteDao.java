package Modelo;
import Controlador.ParticipanteDto;
import Controlador.RazaDto;
import sql.Conexion;
import java.util.ArrayList;
import java.sql.*;
import java.util.List;

public class ParticipanteDao {
    
    public boolean addParticipante(ParticipanteDto dto)
    {   boolean resp=false;
        try{
            Connection conexion = Conexion.getConexion();
            String query="INSERT INTO participante(nombre_participante,"
                    + " id_registro, fecha_registro, id_raza) VALUES(?,?,?,?)";
            PreparedStatement ingresar=conexion.prepareStatement(query);
            //ingresar.setString(1,String.valueOf(dto.getIdParticipante()));
            ingresar.setString(1, dto.getNombre_participante());
            ingresar.setString(2, String.valueOf(dto.getIdRegistro()));
            ingresar.setDate(3, new java.sql.Date(
                    dto.getFechaRegistro().getTime()));
            ingresar.setString(4, String.valueOf(dto.getIdRaza()));
            resp=ingresar.execute();
            conexion.close();
        }catch(SQLException z){
            System.out.println("Error SQL al ingresar "+z.getMessage());
        }catch(Exception q){
            System.out.println("Error al ingresar "+q.getMessage());
        }
        return resp;
    }
    
    
//        public ArrayList<SismoDto> buscarPorMagnitud(int 
//            desde, int hasta)
//    {
//        ArrayList<SismoDto> lista= new ArrayList<SismoDto>();
//        SismoDto dto=null;
//        try{
//            Connection conexion = Conexion.getConexion();
//            String query="SELECT * FROM Sismo WHERE si_magnitud "+
//                   " BETWEEN ? AND ?"; 
//            PreparedStatement buscar=conexion.prepareStatement(query);
//            buscar.setInt(1, desde);
//            buscar.setInt(2, hasta);
//            ResultSet rs = buscar.executeQuery();
//            while(rs.next()){
//                dto = new SismoDto();
//                dto.setEpicentro(rs.getString("si_epicentro"));
//                dto.setEscala(rs.getString("si_escala"));
//                dto.setFecha(rs.getDate("si_fecha"));
//                dto.setHora(rs.getString("si_hora"));
//                dto.setMagnitud(rs.getInt("si_magnitud"));
//                lista.add(dto);
//            }
//            conexion.close();
//        }catch(SQLException w){
//            System.out.println("Error SQL al buscar "+w.getMessage());
//        }catch(Exception z){
//            System.out.println("Error al buscar "+z.getMessage());
//        }
//        return lista;
//    }//fin metodo buscar por magnitud
    
    
     public static  List<ParticipanteDto> listaParticipantes()
    {
        ArrayList<ParticipanteDto> lista= new ArrayList<ParticipanteDto>();
        
        try{
            Connection conexion = Conexion.getConexion();
            String query="select * from participante ORDER by nombre_participante ASC";
            
            PreparedStatement buscar=conexion.prepareStatement(query);           
            ResultSet rs = buscar.executeQuery();
            while(rs.next()){
                ParticipanteDto participante = new ParticipanteDto();
                //lista.add(rs.getString("nombre_participante"));
                participante.setIdRegistro(Integer.parseInt(rs.getString("id_participante")));
                participante.setNombre_participante(rs.getString("nombre_participante"));
                participante.setIdRaza(Integer.parseInt(rs.getString("id_raza")));
                lista.add(participante);
                 //Date d = new Date(Long.parseLong(rs.getString("fecha_registro"))  );
//                 Date d = new Date();
//                participante.setFechaRegistro(d);
                
            }
            conexion.close();
        }catch(SQLException w){
            System.out.println("Error SQL al buscar "+w.getMessage());
        }catch(Exception z){
            System.out.println("Error al buscar "+z.getMessage());
        }
        return lista;
    }
     
     
     
//      public ArrayList<SismoDto> buscarPorEpicentro(String epicentro)          
//    {
//        ArrayList<SismoDto> lista= new ArrayList<SismoDto>();
//        SismoDto dto=null;
//        try{
//            Connection conexion = Conexion.getConexion();
//            String query="SELECT * FROM Sismo WHERE si_epicentro=?";                   
//            PreparedStatement buscar=conexion.prepareStatement(query);
//            buscar.setString(1,epicentro);
//            ResultSet rs = buscar.executeQuery();
//            while(rs.next()){
//                dto = new SismoDto();
//                dto.setEpicentro(rs.getString("si_epicentro"));
//                dto.setEscala(rs.getString("si_escala"));
//                dto.setFecha(rs.getDate("si_fecha"));
//                dto.setHora(rs.getString("si_hora"));
//                dto.setMagnitud(rs.getInt("si_magnitud"));
//                lista.add(dto);
//            }
//            conexion.close();
//        }catch(SQLException w){
//            System.out.println("Error SQL al buscar "+w.getMessage());
//        }catch(Exception z){
//            System.out.println("Error al buscar "+z.getMessage());
//        }
//        return lista;
//    }//fin metodo buscar por epicentro
    
}
