package Modelo;
import Controlador.RazaDto;
import sql.Conexion;
import java.util.ArrayList;
import java.sql.*;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.List;

/**
 *
 * @author camil
 */
public class RazaDao {
         public static List<RazaDto> listaRazas()
    {
               List<RazaDto> listaRaza = new ArrayList<RazaDto>();

        try{
            Connection conexion = Conexion.getConexion();
            String query="select * from raza";
            
            PreparedStatement buscar=conexion.prepareStatement(query);           
            ResultSet rs = buscar.executeQuery();
            while(rs.next()){
               RazaDto raza = new RazaDto();
               raza.setIdRaza(rs.getInt("id_raza"));
               raza.setNombreRaza(rs.getString("nombre_raza"));
               listaRaza.add(raza);
            }
            conexion.close();
        }catch(SQLException w){
            System.out.println("Error SQL al buscar "+w.getMessage());
        }catch(Exception z){
            System.out.println("Error al buscar "+z.getMessage());
        }
        return listaRaza;
    }
}
