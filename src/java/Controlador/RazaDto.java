package Controlador;

import java.util.Date;

public class RazaDto {
    private int idRaza;
    private String nombreRaza;
    

    public RazaDto() {
            this.idRaza=0;
        this.nombreRaza=new String();
    }

    public int getIdRaza() {
        return idRaza;
    }

    public void setIdRaza(int idRaza) {
        this.idRaza = idRaza;
    }

    public String getNombreRaza() {
        return nombreRaza;
    }

    public void setNombreRaza(String nombreRaza) {
        this.nombreRaza = nombreRaza;
    }

    @Override
    public String toString() {
        return "RazaDto{" + "idRaza=" + idRaza + ", nombreRaza=" + nombreRaza + '}';
    }


    
}
