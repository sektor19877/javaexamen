package Controlador;
import java.util.Date;
//Debe permitir el ingreso de Participantes, solicitando el nombre, Raza y el ID de Registro del Participante.  
public class ParticipanteDto {
     private int idParticipante;
     private String nombre_participante;
     private int idRegistro;
     private int idRaza;
     private Date fechaRegistro;

    public ParticipanteDto() {
         this.idParticipante=0;
        this.nombre_participante=new String();
        this.idRegistro=0;
        this.idRaza=0;
        this.fechaRegistro= new Date();
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public int getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(int idParticipante) {
        this.idParticipante = idParticipante;
    }

    public String getNombre_participante() {
        return nombre_participante;
    }

    public void setNombre_participante(String nombre_participante) {
        this.nombre_participante = nombre_participante;
    }

    public int getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(int idRegistro) {
        this.idRegistro = idRegistro;
    }

    public int getIdRaza() {
        return idRaza;
    }

    public void setIdRaza(int idRaza) {
        this.idRaza = idRaza;
    }

    @Override
    public String toString() {
        return "ParticipanteDto{" + "idParticipante=" + idParticipante + ", nombre_participante=" + nombre_participante + ", idRegistro=" + idRegistro + ", idRaza=" + idRaza + '}';
    }



}